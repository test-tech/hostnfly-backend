input = {
  "listings": [
    { "id": 1, "num_rooms": 2 },
    { "id": 2, "num_rooms": 1 },
    { "id": 3, "num_rooms": 3 }
  ],
  "bookings": [
    { "id": 1, "listing_id": 1, "start_date": "2016-10-10", "end_date": "2016-10-15" },
    { "id": 2, "listing_id": 1, "start_date": "2016-10-16", "end_date": "2016-10-20" },
    { "id": 3, "listing_id": 2, "start_date": "2016-10-15", "end_date": "2016-10-20" }
  ],
  "reservations": [
    { "id": 1, "listing_id": 1, "start_date": "2016-10-11", "end_date": "2016-10-13" },
    { "id": 1, "listing_id": 1, "start_date": "2016-10-13", "end_date": "2016-10-15" },
    { "id": 1, "listing_id": 1, "start_date": "2016-10-16", "end_date": "2016-10-20" },
    { "id": 3, "listing_id": 2, "start_date": "2016-10-15", "end_date": "2016-10-18" }
  ]
}

module Hostnfly
  require 'json'
  
  class Input
    attr_reader :listing, :bookings, :reservations

    def initialize(listing, input = {})
      sort = -> (item) { item[:listing_id] == listing[:id] }

      @listing = Listing.new(listing)
      @bookings = input[:bookings].select(&sort).map { |booking| Booking.new(booking) }
      @reservations = input[:reservations].select(&sort).map { |reservation| Reservation.new(reservation) }
    end
  end

  class Listing
    attr_reader :id, :num_rooms

    def initialize(input)
      @id = input[:id]
      @num_rooms = input[:num_rooms]
    end
  end

  class Period
    attr_reader :start, :end

    def initialize(input)
      @start = input[:start_date]
      @end = input[:end_date]
    end
  end

  class Step
    attr_reader :label, :price, :date

    def initialize(label, price, date)
      @label = label
      @price = price
      @date = date
    end
  end

  class Booking
    attr_reader :period, :steps

    CHECKIN_LABEL = :first_checkin
    CHECKOUT_LABEL = :last_checkout

    def initialize(input)
      @period = Period.new(input)
      @steps = [Step.new(CHECKIN_LABEL, 10, period.start), Step.new(CHECKOUT_LABEL, 5, period.end)]
    end
  end

  class Reservation
    attr_reader :period, :steps

    CHECKOUT_LABEL = :checkout_checkin

    def initialize(input)
      @period = Period.new(input)
      @steps = [Step.new(CHECKOUT_LABEL, 10, period.end)]
    end
  end

  class Mission
    attr_reader :listing_id, :mission_type, :date, :price

    def initialize(listing, step)
      raise ArgumentError.new('Mission class need an Listing instance arg') if ! listing.is_a? Listing
      raise ArgumentError.new('Mission class need an Step instance arg') if ! step.is_a? Step

      @listing_id = listing.id
      @mission_type = step.label
      @date = step.date
      @price = step.price * listing.num_rooms
    end

    def to_h
      {
        listing_id: listing_id,
        mission_type: mission_type,
        date: date,
        price: price
      }
    end
  end

  class MissionGenerator
    attr_reader :listing, :bookings, :reservations

    def initialize(input)
      raise ArgumentError.new('MissionGenerator class need an Input instance arg') if ! input.is_a? Input

      @listing = input.listing
      @bookings = input.bookings
      @reservations = input.reservations
    end

    def all
      bookings = @bookings.reduce([]) do |bookings, booking|
        booking.steps.map do |step|
          bookings << Mission.new(@listing, step).to_h
        end

        bookings
      end

      reservations = @reservations.reduce([]) do |reservations, reservation|
        reservation.steps.map do |step|
          if bookings.find { |booking| booking[:mission_type] == Booking::CHECKOUT_LABEL && booking[:date] == step.date } .nil?
            reservations << Mission.new(@listing, step).to_h
          end
        end .compact

        reservations
      end

      bookings + reservations
    end
  end

end

def handle_cleanings(input)
  JSON.pretty_generate({
    missions: input[:listings].map do |listing|
      Hostnfly::MissionGenerator.new(Hostnfly::Input.new(listing, input)).all
    end .flatten
  })
end

puts handle_cleanings(input)
